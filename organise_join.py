#!/usr/bin/env python 
# coding: utf-8

#vers
filelist=[
"result.txt",
"pass.txt",
"join.txt"
]

#imports
import sys
import codecs
import re

fresult=codecs.open(filelist[0],'r','utf-8')
fpass=codecs.open(filelist[1],'r','utf-8')
fout=codecs.open(filelist[2],'a','utf-8')

results=fresult.readlines()
passes=fpass.readlines()

fresult.close()
fpass.close()

for res in results:
	for pas in passes:
		p=re.search("(\".*\"),(\".*\")",pas)
		if p:
			if res.startswith(p.group(1)):
				print "%s%s" % (res.strip(),p.group(2))
				fout.write("%s%s\n" % (res.strip(),p.group(2)))
				break

fout.close()
print "Finished!"