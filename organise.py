#!/usr/bin/env python 
# coding: utf-8

#vers
numb=15 #important

filelist=[
"result.txt",
"pass.txt"
]

#imports
import sys
import codecs

for inputfile in filelist:
	fout=codecs.open(inputfile,'a','utf-8')

	for nu in xrange(numb):
		try:
			fin=codecs.open(str(nu)+inputfile,'r','utf-8')#
		except:
			#break
			continue
		lines = fin.readlines()
		if lines:
			for line in lines:
				fout.write(u"%s" % line)
			if "\n" not in line:
				fout.write("\r\n")
		#fout.writelines(lines)
		#fout.write("\n")
		fin.close()
		#nu=nu+1#ends
	
	#fin.close()
	fout.close()
	print "Finished!"